Ex 3, Session 5: Communications using services

Ex 3a:
Make a copy of pubvel_toggle_plus.cpp and name it improved_pubvel_toggle.cpp.
Modify this new file to include:
  - A service to start/stop the turtle.
  - A service that allows to change the speed.

Ex 3b:
Make a copy of spawn_turtle.cpp and name it spawn_turtle_plus.cpp. Modify this new file in
order that, besides calling the /spawn service to create a new turtle called MyTurtle,
this node also:
  - Subscribes to the topic turtle1/cmd_vel (that will be provided by the pubvel_toggle_plus node).
  - Publishes the received command velocity to the topic MyTurtle/cmd_vel in order to move both
  of the turtles simultaneously.
Hint: Create the publisher as a global pointer, initialize it in the main function and used it
in the subscriber callback function.

Ex 3c(Optional):
Create a launch file that runs:
  - the turtlesim and improved_pubvel_toggle nodes
  - and, optionally, also runs the spawn_turtle_plus node of Exercise 3b, by using an argument.
