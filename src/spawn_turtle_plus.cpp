// Ex 3b
//This program spawns a new turtlesim turtle by calling
// the appropriate service.
#include <ros/ros.h>
//The srv class for the service.
#include <turtlesim/Spawn.h>
#include <geometry_msgs/Twist.h>    // For geometry_msgs::Twist

ros::Publisher *pubVel;

void velMessageReceived(const geometry_msgs::Twist& msgIn){
    geometry_msgs::Twist msgOut;
    msgOut.linear.x = msgIn.linear.x;
    msgOut.angular.z = msgIn.angular.z;
    pubVel -> publish(msgOut);
}

int main(int argc, char **argv){

    ros::init(argc, argv, "spawn_turtle");
    ros::init(argc, argv, "publish_vel_control_TungTurtle");
    ros::init(argc, argv, "subcribes_vel_from_turtle1");
    ros::NodeHandle nh;

//Create a client object for the spawn service. This
//needs to know the data type of the service and its name.
    ros::ServiceClient spawnClient
                = nh.serviceClient<turtlesim::Spawn>("spawn");

//Create the request and response objects.
    turtlesim::Spawn::Request req;
    turtlesim::Spawn::Response resp;

    req.x = 5;
    req.y = 5;
    req.theta = M_PI/2;
    req.name = "TungTurtle";

    ros::service::waitForService("spawn", ros::Duration(10));
    bool success = spawnClient.call(req,resp);

    if(success){
        ROS_INFO_STREAM("Spawned a turtle named "
                        << resp.name);
    }else{
        ROS_ERROR_STREAM("Failed to spawn.");
    }

    while (ros::ok()){
        // subcriber vel turtle1
        ros::Subscriber sub = nh.subscribe(
           "turtle1/cmd_vel", 1000,
           &velMessageReceived);

//        pubVel = new ros::Publisher(
//            nh.advertise<geometry_msgs::Twist>(
//              "turtle1/cmd_vel",
//              1000));

        // control Tung's turtle
        pubVel = new ros::Publisher(
            nh.advertise<geometry_msgs::Twist>(
              "TungTurtle/cmd_vel",
              1000));

        ros::spin();
        delete pubVel;
    }

}
