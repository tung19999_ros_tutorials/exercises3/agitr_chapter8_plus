//Ex 3a
//this program toggles between rotation and translation
//commands, change speed, status start-stop,
//based on calls to a service.
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <agitr_chapter8_plus/Changespeed.h>

bool forward = true;
bool start = true;
double speed = 1.0;         // default speed
bool speedchanged = false;

bool toggleForward(
        std_srvs::Empty::Request &req,
        std_srvs::Empty::Response &resp){
        forward = !forward;
        ROS_INFO_STREAM("Now sending "<<(forward?
                "forward":"rotate")<< " commands.");
        return true;
}

bool startStop(
        std_srvs::Empty::Request &req,
        std_srvs::Empty::Response &resp){
        start = !start;
        ROS_INFO_STREAM("Turtle:  "<<(start?
                "Start":"Stop")<< " commands.");
        return true;
}

bool changeSpeed(
        agitr_chapter8_plus::Changespeed::Request &req,
        agitr_chapter8_plus::Changespeed::Response &resp){

        ROS_INFO_STREAM("Changing Turtle Speed:  "<< req.newSpeed);

        speed = req.newSpeed;
        speedchanged = true;

        return true;
}

int main(int argc, char **argv){
        ros::init(argc,argv,"pubvel_toggle");
        ros::NodeHandle nh;

        ros::ServiceServer server =
                nh.advertiseService("toggle_forward",&toggleForward);

        ros::ServiceServer server1 =
                nh.advertiseService("start_stop",&startStop);

        ros::ServiceServer server2 =
                nh.advertiseService("change_speed",&changeSpeed);

        ros::Publisher pub=nh.advertise<geometry_msgs::Twist>(
                "turtle1/cmd_vel",1000);

        ros::Rate rate(10);
        while(ros::ok()){
                geometry_msgs::Twist msg;
                if (start == 1){
                    if (speedchanged){
                        speedchanged = false;
                    }
                    msg.linear.x = forward?speed:0.0;
                    msg.angular.z=forward?0.0:speed;
                }
                else{
                    msg.linear.x = 0;
                    msg.angular.z =0;
                }

                pub.publish(msg);
                ros::spinOnce();
                rate.sleep();
        }
}
